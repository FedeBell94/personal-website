import React from 'react';
import { Button } from '@material-ui/core';

import Colors from '../styles/colors';

type Props = {
    children: React.ReactChild,
    color: string,
}

const StyledButton = ({ children, color }: Props) => {
    const style = {
        backgroundColor: color,
        color: Colors.text_primary,
        outlineWidth: 0,
        fontFamily: 'Ubuntu',
    };

    return (
        <Button variant='contained' style={style}>
            {children}
        </Button>
    );
};


export default StyledButton;